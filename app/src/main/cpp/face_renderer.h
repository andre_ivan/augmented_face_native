//
// Created by andre on 20/09/16.
//

#ifndef COMPUTERVISION_C_FACE_RENDERER_H
#define COMPUTERVISION_C_FACE_RENDERER_H

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <android/asset_manager.h>
#include <media/NdkImage.h>
#include <memory>

#include "arcore_c_api.h"
#include "util.h"
#include "glm.h"

namespace computer_vision {
    class FaceRenderer {
    public:
        FaceRenderer() = default;
        ~FaceRenderer() = default;

        void InitializeGlContent(AAssetManager* asset_manager, const std::string& png_file_name);

        void Draw(const ArSession* session, ArTrackable* face, glm::mat4& projmtx, glm::mat4& viewmtx, glm::mat4& modelmtx, float* color_correction);

        GLuint GetTextureId() const;

    private:
        GLuint shader_program_;
        GLuint load_tex;

        GLuint texture_id_;

        GLint uniform_mvp_mat_;
        GLint uniform_model_view_;
        GLint uniform_texture;

        GLint uniform_light_params;
        GLint uniform_material_params;

        GLint attrib_vertices;
        GLint attrib_uvs;
        GLint attrib_normals;
        GLint uniform_color_correction_params;
        GLint uniform_tint_params;

        float ambient = 0.3f;
        float diffuse = 1.0f;
        float specular = 1.0f;
        float specularPower = 6.0f;
    };



} // namespace computer_vision

#endif //COMPUTERVISION_C_FACE_RENDERER_H
