//
// Created by andre on 20/09/16.
//
#include "face_renderer.h"
#include "util.h"

namespace computer_vision {
namespace {
    constexpr char kVertexShaderFilename[] = "shaders/object.vert";
    constexpr char kFragmentShaderFilename[] = "shaders/object.frag";
    constexpr char textureFilename[] = "models/freckles.png";
    const glm::vec4 kLightDirection(0.0f, 1.0f, 0.0f, 0.0f);
}  // namespace

    void FaceRenderer::InitializeGlContent(AAssetManager *asset_manager, const std::string& png_file_name)
    {

        shader_program_ = util::CreateProgram(asset_manager, kVertexShaderFilename,
                                              kFragmentShaderFilename);

        if (!shader_program_) {
            LOGE("Could not create program.");
        }

        uniform_mvp_mat_ = glGetUniformLocation(shader_program_, "u_ModelViewProjection");
        uniform_model_view_ = glGetUniformLocation(shader_program_, "u_ModelView");
        uniform_texture = glGetUniformLocation(shader_program_, "u_Texture");

        uniform_light_params = glGetUniformLocation(shader_program_, "u_LightningParameters");
        uniform_material_params = glGetUniformLocation(shader_program_, "u_MaterialParameters");


        uniform_color_correction_params = glGetUniformLocation(shader_program_, "u_ColorCorrectionParameters");
        uniform_tint_params = glGetUniformLocation(shader_program_, "u_TintColor");

        attrib_vertices = glGetAttribLocation(shader_program_, "a_Position");
        attrib_uvs = glGetAttribLocation(shader_program_, "a_TexCoord");
        attrib_normals = glGetAttribLocation(shader_program_, "a_Normal");

        glActiveTexture(GL_TEXTURE0);
        GLuint textures[1];
        glGenTextures(1, textures);
        texture_id_ = textures[0];

        // Loading image using Google util function does not work(?)
//        if (!util::LoadPngFromAssetManager(GL_TEXTURE_2D, png_file_name)) {
//            LOGE("Could not load png texture.");
//        }
//        glGenerateMipmap(GL_TEXTURE_2D);
//
//        glBindTexture(GL_TEXTURE_2D, 0);

    }

    void FaceRenderer::Draw(const ArSession* session, ArTrackable* face, glm::mat4& projection_mat, glm::mat4& view_mat, glm::mat4& model_mat, float* color_correction)
    {
        //// https://chromium.googlesource.com/external/github.com/google-ar/arcore-android-sdk/+/refs/heads/master/libraries/include/arcore_c_api.h#3716
        //// https://developers.google.com/ar/develop/java/augmented-faces/developer-guide
        const uint16_t* indices = 0;
        int32_t trianglesCount = 0;
        ArAugmentedFace_getMeshTriangleIndices(session, ArAsFace(face), &indices, &trianglesCount);
        //LOGE("Triangles = %d", trianglesCount);

        const float* textureCoords = 0;
        int32_t textureCoordsCount = 0;
        ArAugmentedFace_getMeshTextureCoordinates(session, ArAsFace(face),  &textureCoords, &textureCoordsCount);

        int32_t count = 0;
        const float* vertices = 0;
        const float* normals = 0;
        ArAugmentedFace_getMeshVertices(session, ArAsFace(face), &vertices, &count);
        ArAugmentedFace_getMeshNormals(session, ArAsFace(face), &normals, &count);

        glUseProgram(shader_program_);
        glDepthMask(GL_FALSE);

        glm::mat4 mvp_mat = projection_mat * view_mat * model_mat;
        glm::mat4 mv_mat = view_mat * model_mat;
        glm::vec4 view_light_direction = glm::normalize(mv_mat * kLightDirection);

        glUniform4f(uniform_light_params, view_light_direction[0],
                    view_light_direction[1], view_light_direction[2], 1.f);

        glUniform4f(uniform_color_correction_params, color_correction[0],
                    color_correction[1], color_correction[2], color_correction[3]);

        // Set the object material properties.
        glUniform4f(uniform_material_params, ambient, diffuse, specular, specularPower);

        // Set the ModelViewProjection matrix in the shader.
        glUniformMatrix4fv(uniform_model_view_, 1, GL_FALSE, glm::value_ptr(mv_mat));
        glUniformMatrix4fv(uniform_mvp_mat_, 1, GL_FALSE, glm::value_ptr(mvp_mat));

        glEnableVertexAttribArray(attrib_vertices);
        glVertexAttribPointer(attrib_vertices, 3, GL_FLOAT, GL_FALSE, 0, vertices);

        glEnableVertexAttribArray(attrib_normals);
        glVertexAttribPointer(attrib_normals, 3, GL_FLOAT, GL_FALSE, 0, normals);

        glEnableVertexAttribArray(attrib_uvs);
        glVertexAttribPointer(attrib_uvs, 2, GL_FLOAT, GL_FALSE, 0, textureCoords);

        glActiveTexture(GL_TEXTURE0);
        glUniform1i(uniform_texture, 0);

        glBindTexture(GL_TEXTURE_2D, texture_id_);
        glUniform4f(uniform_tint_params, 0, 0, 0, 0);
        glEnable(GL_BLEND);

        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        glDrawElements(GL_TRIANGLES, trianglesCount*2.5, GL_UNSIGNED_SHORT, indices); // multiply by 3 to draw whole indices
        glUseProgram(0);
        glDepthMask(GL_TRUE);
        util::CheckGlError("FaceRenderer::Draw() error");

    }


} // namespace computer_vision
